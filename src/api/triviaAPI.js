

export function fetchQuestions(){
    return fetch('https://opentdb.com/api.php?amount=5&category=9&difficulty=easy')
            .then(response => response.json())
}