import Vue from 'vue'
import App from './App'
import router from "@/router";
import VueRouter from "vue-router";
import './main.css'

Vue.use(VueRouter)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
