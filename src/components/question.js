// Stores the questions and the answers

class Question {

    constructor(qNumber, question, type, correct_answer, incorrect_answers){
        this.qNumber = qNumber
        this.question = question
        if (type === 'multiple'){
            this.answers = {
                0: correct_answer,
                1: incorrect_answers[0],
                2: incorrect_answers[1],
                3: incorrect_answers[2]
            }
        } else{
            this.answers = {
                0: correct_answer,
                1: incorrect_answers[0]
            }
        }
    }
    setAnswer = (a) => {this.submitAnswer = a}

    getAnswerSubmit = () => {return this.submitAnswer}

    answerIsRight = () => {return this.getAnswerSubmit===this.answers[0]}

    shuffle (){
        let array = this.answers
        for (let i = array.length - 1; i > 0; i--) {
            let rand = Math.floor(Math.random() * (i + 1));
            [array[i], array[rand]] = [array[rand], array[i]]
        }
        return array
    }
}

export function makeQuestionList(results){

    let quiz = []

    for (let i = 0; i < results.length; i++){
        quiz.push(
            new Question(
                i+1,
                results[i].question,
                results[i].type,
                results[i].correct_answer,
                results[i].incorrect_answers
            )
        )
    }
    return quiz
}