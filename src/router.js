import VueRouter from "vue-router";
import Question from "@/components/Question/Question";
import StartScreen from "@/components/StartScreen";

const routes = [
    {
        path: '/',
        component: StartScreen
    },
    {
        path: '/start',
        name: 'startScreen',
        component: StartScreen
    },
    {
        path: '/quiz',
        name: 'question',
        component: Question
    },

]

const router = new VueRouter({routes})

export default router