# viu-trivia

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).



API fetch:

created() {
fetchQuestions().then(questions => {
console.log(questions)
this.questions = questions.results

    }).catch(error => {
      this.error = error
      console.log(error.message)
    })
},
data() {
return {
questions: [],
error: ''
}
}